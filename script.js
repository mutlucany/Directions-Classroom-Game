(() => {
  let emoji;
  let name;
  const lobby = document.getElementById('lobby');
  const lobbylist = document.getElementById('lobbylist');
  const welcome = document.getElementById('welcome');
  const results = document.getElementById('results');
  const form = document.getElementById('form');
  const board = document.getElementById('board');
  const player = document.getElementById('player');
  const playerIcon = document.getElementById('player-icon');
  const buttonsDiv = document.getElementById('buttons');
  const checkpoint1Div = document.getElementById('checkpoint1');
  const checkpoint2Div = document.getElementById('checkpoint2');
  const b1 = document.getElementById('b1');
  const b2 = document.getElementById('b2');
  const b3 = document.getElementById('b3');
  const b4 = document.getElementById('b4');
  const elm = board;

  const buttons = ['l', 'r', 's', 'x'];
  let direction = 'n';
  let checkpoint1 = false;
  let checkpoint2 = false;
  let x = 500;
  let y = 600;

  const shuffleArray = (array) => {
    for (let i = array.length - 1; i > 0; i--) {
      const j = Math.floor(Math.random() * (i + 1));
      [array[i], array[j]] = [array[j], array[i]];
    }
  };

  const updateButtons = () => {
    b1.className = buttons[0];
    b2.className = buttons[1];
    b3.className = buttons[2];
    b4.className = buttons[3];
    shuffleArray(buttons);
  };

  updateButtons();

  const goStraight = () => {
    let newY;
    let newX;
    switch (direction) {
      case 'n':
        newY = y - 100;
        newX = x;
        break;
      case 'w':
        newX = x - 100;
        newY = y;
        break;
      case 's':
        newY = y + 100;
        newX = x;
        break;
      case 'e':
        newX = x + 100;
        newY = y;
        break;
      default:
        break;
    }

    if ( newX !== 700 &&
        newX !== -100 &&
        newY !== 700 &&
        newY !== -100 &&
        !(newX === 0 && newY === 0) &&
        !(newX === 200 && newY === 0) &&
        !(newX === 300 && newY === 0) &&
        !(newX === 400 && newY === 0) &&
        !(newX === 600 && newY === 0) &&
        !(newX === 0 && newY === 200) &&
        !(newX === 100 && newY === 200) &&
        !(newX === 200 && newY === 200) &&
        !(newX === 400 && newY === 200) &&
        !(newX === 600 && newY === 200) &&
        !(newX === 0 && newY === 400) &&
        !(newX === 200 && newY === 400) &&
        !(newX === 300 && newY === 400) &&
        !(newX === 400 && newY === 400) &&
        !(newX === 600 && newY === 400) &&
        !(newX === 0 && newY === 600) &&
        !(newX === 100 && newY === 600) &&
        !(newX === 200 && newY === 600) &&
        !(newX === 400 && newY === 600) &&
        !(newX === 600 && newY === 600)
    ) {
      x = newX;
      y = newY;
      player.style.left = x + 'px';
      player.style.top = y + 'px';

      firebase.database().ref('/directions/players/'+emoji).update({
        x: x,
        y: y,
      });

      if ((x === 500 && y === 0) || (x === 400 && y === 100)) {
        checkpoint1 = true;
        checkpoint1Div.classList.remove('hidden');
      }

      if ((x === 0 && y === 100) || (x === 0 && y === 300)) {
        checkpoint2 = true;
        checkpoint2Div.classList.remove('hidden');
      }

      if ((x === 500 && y === 600) || (x === 600 && y === 500)) {
        if (checkpoint1 && checkpoint2) {
          finish();
        }
      }
    }
  };

  const turnLeft = () => {
    switch (direction) {
      case 'n':
        direction = 'w';
        player.style.transform = 'rotate(270deg)';
        break;
      case 'w':
        direction = 's';
        player.style.transform = 'rotate(180deg)';
        break;
      case 's':
        direction = 'e';
        player.style.transform = 'rotate(90deg)';
        break;
      case 'e':
        direction = 'n';
        player.style.transform = 'rotate(0deg)';
        break;
      default:
        // statements_def
        break;
    }
  };

  const turnRight = () => {
    switch (direction) {
      case 's':
        direction = 'w';
        player.style.transform = 'rotate(270deg)';
        break;
      case 'e':
        direction = 's';
        player.style.transform = 'rotate(180deg)';
        break;
      case 'n':
        direction = 'e';
        player.style.transform = 'rotate(90deg)';
        break;
      case 'w':
        direction = 'n';
        player.style.transform = 'rotate(0deg)';
        break;
      default:
        // statements_def
        break;
    }
  };

  buttonsDiv.addEventListener('pointerup', (e) => {
    switch (e.target.className) {
      case 'l':
        turnLeft();
        break;
      case 'r':
        turnRight();
        break;
      case 's':
        goStraight();
        break;
      default:
        turnRight();
        break;
    }
    updateButtons();
  });

  const finish = () => {
    results.classList.remove('hidden');
    const newPostRef = firebase.database().ref('/directions/results').push();
    newPostRef.set({
      name: name,
      emoji: emoji,
      time: firebase.database.ServerValue.TIMESTAMP,
    });
  };

  firebase.database().ref('/directions/results').orderByChild('time').on('child_added', (data) => {
    const val = data.val();
    const node = document.createElement('li');
    const textnode = document
        .createTextNode(val.emoji + ' ' + val.name);
    node.appendChild(textnode);
    results.appendChild(node);
  });

  firebase.database().ref('/directions/players').on('child_added', (data) => {
    const val = data.val();
    if (val.emoji !== emoji) {
      const node = document.createElement('div');
      const textnode = document
          .createTextNode(val.emoji);
      node.id = 'p' + val.emoji;
      node.className = 'remote-player';
      node.appendChild(textnode);
      board.appendChild(node);

      firebase.database().ref('/directions/players/'+val.emoji).on('value', (snapshot) => {
        const data = snapshot.val();
        if (data.x && data.y) {
          node.style.left = data.x + 'px';
          node.style.top = data.y + 'px';
        }
      });
    }
  });

  firebase.database().ref('/directions/state')
      .once('value').then((snapshot) => {
        const state = snapshot.val();
        if (state !== 'join') {
          alert('You cannot join at this time');
          form.classList.add('hidden');
          results.classList.remove('hidden');
        }
      }).catch(() => {
        alert('Cannot reach Google, try again');
      });

  firebase.database().ref('/directions/state').on('value', (snapshot) => {
    const val = snapshot.val();
    if (val === 'play') {
      lobby.classList.add('hidden');
      welcome.classList.add('hidden');
    } else if (val === 'end') {
      results.classList.add('finish');
    }
  });

  form.addEventListener('submit', () => {
    document.body.requestFullscreen();
    emoji = document.getElementById('emoji').value;
    name = document.getElementById('name').value;
    playerIcon.textContent = emoji;
    firebase.database().ref('/directions/players/'+emoji)
        .once('value').then((snapshot) => {
          const val = snapshot.val();
          if (val) {
            alert('Emoji already in use');
          } else {
            firebase.database().ref('/directions/players/' + emoji).set({
              name: name,
              emoji: emoji,
              x: x,
              y: y,
            }, function(error) {
              if (error) {
                alert(error);
              } else {
                welcome.classList.add('hidden');
                firebase.database().ref('/directions/players')
                    .on('child_added', function(data) {
                      const node = document.createElement('li');
                      const textnode = document
                          .createTextNode(data.key + ' ' + data.val().name);
                      node.appendChild(textnode);
                      lobbylist.appendChild(node);
                    });
              }
            });
          }
        }).catch(() => {
          alert('Cannot reach Google, try again');
        });
  });

  setInterval(() => {
    elm.style.transform =
        'scale('+1/Math.max(700/window.innerWidth, 700/window.innerHeight)+')';
  }, 2000);
})();

