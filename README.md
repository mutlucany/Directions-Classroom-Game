# Directions Classroom Game
A multi-player game to teach English words for directions suitable for in-person or remote classroom environments.

### Demo Page
For players: <https://mutlulabs.web.app/directionsgame/>

For the presenter: <https://mutlulabs.web.app/directionsgame/board/>

**Notice:** This software is not ready for the end user. If you're a teacher without any knowledge of the software (JavaScript and Firebase) feel free to reach me at <mtlcnylmz@gmail.com>. I will help free of charge provided that you are not working for a private school.


## How to play
- Everyone enters the web app via any device with a browser,
- Enters their name,
- Chooses an emoji as a game character.
- The character should be directed to pass by each starred **checkpoint** and **go back to home** by pressing buttons.
- Buttons swap places to force to look for meaning and for fun.

## The Code
This app uses **Firebase Realtime Database**. It is intended for one session. There is no administration panel. Just change `/directions/state` to `join` or `play`.

## Screenshots
![ss1.png](ss1.png)
![ss2.png](ss2.png)
