(() => {
  const lobbylist = document.getElementById('lobbylist');
  const results = document.getElementById('results');
  const board = document.getElementById('board');

  firebase.database().ref('/directions/results').orderByChild('time').on('child_added', (data) => {
    const val = data.val();
    const node = document.createElement('li');
    const textnode = document
        .createTextNode(val.emoji + ' ' + val.name);
    node.appendChild(textnode);
    results.appendChild(node);
  });

  firebase.database().ref('/directions/players').on('child_added', (data) => {
    const val = data.val();
    const node = document.createElement('div');
    const textnode = document
        .createTextNode(val.emoji);
    node.id = 'p' + val.emoji;
    node.className = 'remote-player';
    node.appendChild(textnode);
    board.appendChild(node);

    firebase.database().ref('/directions/players/'+val.emoji).on('value', (snapshot) => {
      const data = snapshot.val();
      if (data.x && data.y) {
        node.style.left = data.x + 'px';
        node.style.top = data.y + 'px';
      }
    });
  });

  firebase.database().ref('/directions/players')
      .on('child_added', function(data) {
        const node = document.createElement('li');
        const textnode = document
            .createTextNode(data.key + ' ' + data.val().name);
        node.appendChild(textnode);
        lobbylist.appendChild(node);
      });
})();

